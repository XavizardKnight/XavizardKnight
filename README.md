# Hi! I'm Xavier Martínez

Welcome to my GitLab profile page.
Thank you for your interest in wanting to read more about me.

I'm a 21yo male who really likes to program C# and I'm slowly but surely learning on my own since 2019.

My basic interests:
- C# programming with Unity3D, AvaloniaUI or Terminal.Gui (gui.cs). And with .NET Standard 2.0 and .NET 5+.
- Linux + X11 + Compiz + Emerald.
- Linux, BSD and other exotic OS; like ReactOS or Haiku. Heavy VM nerd.
- Furry fandom. My main fursona is a dangerfloof (a shapeshifter) furred dragon.
- I like trains and railway sims. I've got in my roadmap (or railmap?) to try to make a railsim somehow someday.
- Acrobatic and military aviation.

## My social networks
Do you want to contact me or see what I do elsewhere? Here's a selection of my most-used social networks. Contact me if you want; but please, [#NoHello](https://nohello.net/en/):

<!--<style>
    td, th
    {
        border: none!important;
    }
</style>-->
<table>
    <tr>
        <td><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/E059_color.png" width=200/></td>
        <td>@XavizardKnight</td>
    <!-- </tr> -->
	<!-- <tr> -->
        <td><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/E046_color.png" width=200/></td>
        <td><a href="https://www.linkedin.com/in/xavier-martinez-sanahuja">Xavier Martínez Sanahuja</a></td>
    </tr>
	<tr>
        <td><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/E05A_color.png" width=200/></td>
        <td><a rel="me" href="https://mastodont.cat/@xavizardknight">@XavizardKnight@mastodont.cat</a></td>
    <!-- </tr> -->
	<!-- <tr> -->
        <td><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/E043_color.png" width=200/></td>
        <td>@xavierms1302</td>
    </tr>
	<tr>
        <!--<td><img src="readme/E05C_color.png" width=50/></td>
        <td><a href="www.gitlab.com/XavizardKnight">@XavizardKnight@pix.mastodont.cat</a></td>-->
    </tr>
</table>
<br/>


## My projects

Here's a list (or a table should I say) of my public repos:

<table>
	<tr>
		<td><!--<a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge">--><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/xk/XK2023-CubesSimple-SquareFilled.png" width="150"/><!--</a>--></td>
		<td>
			<center>
				<b>OpenMoji XK Extensions</b>
				<br>
				<i>(Comming soon)</i>
				<br>
				<img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/svg.png" alt="W3C SVG Logo" width="25"/>
				<img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/cs.png" alt="W3C SVG Logo" width="25"/>
			</center>
		</td>
		<td>
			This repository holds a collection of emojis that I've created in the style of OpenMoji. Contains a bunch of diferent emojis; mainly flags, but also icons, brands.
			Has a .NET 6 Terminal.Gui project that generates diferent versions of these emojis (colour, outline only, white, SVGs, PNGs...)
		</td>
	</tr>
	<tr>
		<td><!--<a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge">--><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/repos/umbrella/image2.png" alt="Game Jolt black badge" width="150"/><!--</a>--></td>
		<td>
			<center>
				<b>Umbrella Versioning</b>
				<br>
				<i>(Comming soon)</i>
			</center>
		</td>
		<td>
			A versioning system that is extremly long and verbose in propuse. It aims to give as many info as possible.
			<br/>An example: "<b>r1 m3 v1.3.0.1 b27 d20230301-1150</b>"
		</td>
	</tr>
	<tr>
		<td><a href="https://gitlab.com/XavizardKnight/XK.Utils"><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/xk/XK2023-CubesSimple-SquareFilled.png" alt="" width="150"/></a></td>
		<td>
			<center>
				<b>XK.Utils</b>
				<br>
				<a href="https://gitlab.com/XavizardKnight/XK.Utils"><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/gitlab-icon-rgb.png" alt="Check GitLab repo" href="https://gitlab.com/XavizardKnight/XK.Utils" width="25"/>
				<img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/cs.png" alt="C#" width="25"/>
			</center>
		</td>
		<td>
			XK.Utils is my personal utility library for .NET Standard 2.0. Contains a bunch of useful methods for all kinds of programs.
			<br>
			<img alt="Stars" src="https://img.shields.io/badge/dynamic/json.svg?style=for-the-badge&label=Stars&url=https://gitlab.com/api/v4/projects/44640408&query=star_count&logo=star&color=orange" href="https://gitlab.com/XavizardKnight/XK.Utils">
			<img src="https://img.shields.io/gitlab/license/XavizardKnight%2FXK.Utils?style=for-the-badge">
			<img alt="GitLab all issues" src="https://img.shields.io/gitlab/issues/open/XavizardKnight%2FXK.Utils?style=for-the-badge">
			<img alt="GitLab all merge requests" src="https://img.shields.io/gitlab/merge-requests/open/XavizardKnight%2FXK.Utils?style=for-the-badge">
		</td>
	</tr>
	<tr>
		<td><a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/repos/unofficial-gj-badge/gj-repo-icon-highres.png" alt="Game Jolt black badge" width="150"/></a></td>
		<td>
			<center>
				<b>unofficial-gj-badge</b>
				<br><a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"><img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/gitlab-icon-rgb.png" alt="Game Jolt black badge" href="https://gitlab.com/XavizardKnight/unofficial-gj-badge" width="25"/></a>
				<img src="https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/readme/svg.png" alt="W3C SVG Logo" width="25"/>
			</center>
		</td>
		<td>
			An unofficial "Available on Game Jolt" badge for your games' trailers and promotional arts.
			<br><a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"><img alt="Stars" src="https://img.shields.io/badge/dynamic/json.svg?style=for-the-badge&label=Stars&url=https://gitlab.com/api/v4/projects/27445856&query=star_count&logo=star&color=orange" href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"></a>
			<img src="https://img.shields.io/gitlab/license/XavizardKnight%2Funofficial-gj-badge?style=for-the-badge">
		</td>
	</tr>
</table>
<br/>

## My games

During my free time I develop and release indie videogames under the name "Xavizard Knight Games". <!--Here are the ones that I've released so far (and the ones that are to be comming soon):-->

<center><a href="https://xavizardknightgames.indie.af">
Check the Xavizard Knight Games webpage hosted in
<br/><u>Indie.af</u> for more information on my videogames.</a></center>

<!--<table>
	<tr>
		<td><img src="xk/XK2023-CubesSimple-SquareFilled.png" width="150"/></td>
		<td>
			<center>
				<b>Five Nightmares</b>
			</center>
		</td>
		<td>
			My reimagination of FNaF4. Survive the night in your bedroom, closing doors and windows to repel attacks from nightmarish creatures. Plan your movements and sleep when no monsters are nearby to pass time faster.
		</td>
	</tr>
	<tr>
		<td><img src="repos/umbrella/image2.png" alt="Game Jolt black badge" width="150"/></td>
		<td>
			<center>
				<b>The Spirit House</b>
			</center>
		</td>
		<td>
			<b>XK.Utils</b> is my personal utility library for .NET Standard 2.0. Contains a
		</td>
	</tr>
	<tr>
		<td><img src="xk/XK2023-CubesSimple-SquareFilled.png" alt="Game Jolt black badge" width="150"/></td>
		<td>
			<center>
				<b>XK.Utils</b>
				<br>
				<img src="readme/cs.png" alt="W3C SVG Logo" width="25"/>
			</center>
		</td>
		<td>
			<b>XK.Utils</b> is my personal utility library for .NET Standard 2.0. Contains a
		</td>
	</tr>
	<tr>
		<td><a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"><img src="repos/unofficial-gj-badge/gj-repo-icon-highres.png" alt="Game Jolt black badge" width="150"/></a></td>
		<td>
			<center>
				<b>unofficial-gj-badge</b>
				<br><a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"><img src="readme/gitlab-icon-rgb.png" alt="Game Jolt black badge" href="https://gitlab.com/XavizardKnight/unofficial-gj-badge" width="25"/></a>
				<img src="readme/svg.png" alt="W3C SVG Logo" width="25"/>
			</center>
		</td>
		<td>
			An unofficial "Available on Game Jolt" badge for your games' trailers and promotional arts.
			<br><a href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"><img alt="Stars" src="https://img.shields.io/badge/dynamic/json.svg?style=for-the-badge&label=Stars&url=https://gitlab.com/api/v4/projects/27445856&query=star_count&logo=star&color=orange" href="https://gitlab.com/XavizardKnight/unofficial-gj-badge"></a>
			<img src="https://img.shields.io/gitlab/license/XavizardKnight%2Funofficial-gj-badge?style=for-the-badge">
		</td>
	</tr>
</table>-->


<!--
https://gitlab.com/XavizardKnight/XavizardKnight/-/raw/master/
-->
